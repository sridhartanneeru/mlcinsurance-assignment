
## Clone a repository

Clone the repository using:
git clone https://sridhartanneeru@bitbucket.org/sridhartanneeru/mlcinsurance.git

## Pre-requisites:
java 8 or above (openjdk)
Maven

## IDE:
Any comfortable IDE from the user's convenience like IntelliJ/Eclipse/VSCode

## Framework:
This framework designed using serenity cucumber BDD with Page Object Model pattern for UI tests and Serenity-Rest Assured integration for API tests

## Running tests:
    Run this command to run all the tests parallel:
        mvn clean test

    Run this command to run all the tests in parallel and generate the aggregate report
        mvn clean test serenity:aggregate

    To run the tests individually using tags
        mvn clean test serenity:aggregate -Dcucumber.options="--tags @mlc" // this is to run the mlc tests(tagged with @mlc) from this repo and generate the serenity aggregated report
        mvn clean test serenity:aggregate -Dcucumber.options="--tags @ato" // this is to run the ato tests from this repo and generate the serenity aggregated report
        mvn clean test serenity:aggregate -Dcucumber.options="--tags @api" // this is to run the api tests from this repo and generate the serenity aggregated report

    Generated report in the following folder:
        target/site/serenity/index.html

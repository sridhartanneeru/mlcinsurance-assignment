package api.cucumber.junit.step;

import features.api.test.GetAmountByWeightApi;
import io.restassured.response.Response;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.Given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

public class GetAmountByWeightStepDef {

    GetAmountByWeightApi getAmountByWeightApi = new GetAmountByWeightApi();
    Response response;

    @Given("^Send an api request with from country \"([^\"]*)\" to country \"([^\"]*)\" and weight \"([^\"]*)\"$")
    public void sendAnApiRequest(String fromCountry, String toCountry, String Weight) {

        response = getAmountByWeightApi.getAmountByWeight("INTERNATIONAL", fromCountry, toCountry, Weight);

    }

    @When("^GET Api returns response with status code \"([^\"]*)\"$")
    public void GETApiReturnsResponse(int expectedStatusCode) throws InterruptedException {

        assertEquals("Returned response status is not matching", response.statusCode(), expectedStatusCode);

    }

    @Then("^Validate the api response with \"([^\"]*)\" for \"([^\"]*)\"$")
    public void ValidateAPIResponse(String price, int weight) throws InterruptedException {
        // to get the list of items for the code INT_PARCEL_COR_OWN_PACKAGING from the response body
        List<Map<String, String>> items = response.jsonPath().getList("items[0].items[5].items[0].items");
        for (int i = 0; i < items.size(); i++) {
            Integer code = Integer.valueOf(items.get(i).get("code"));
            if (code == weight) {
                String val = String.valueOf(items.get(i).get("price"));
                assertTrue(val.equals(price));
                break;
            }
        }
    }

}

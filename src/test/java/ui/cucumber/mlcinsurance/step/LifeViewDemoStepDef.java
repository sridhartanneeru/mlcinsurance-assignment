package ui.cucumber.mlcinsurance.step;

import common.BaseTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import static org.junit.Assert.assertTrue;

public class LifeViewDemoStepDef extends BaseTest {

    @Steps
    private pages.mlcinsurance.HomePage homePage_mlcinsurance;
    private pages.mlcinsurance.LifeViewDemoPage lifeViewDemoPage_mlcinsurance;

    @Given("^Navigate to mlc insurance home page$")
    public void navigateToMLCInsuranceHomePage() {

        homePage_mlcinsurance.navigate();

    }

    @When("^the user searches for \"([^\"]*)\"$")
    public void theUserSearchesFor(String searchWord) {

        homePage_mlcinsurance.searchGlobal(searchWord);
        assertTrue("page header is not displayed as expected for " + searchWord,
                homePage_mlcinsurance.searchResultDisplayed());

    }

    @When("^the user clicks on \"([^\"]*)\" link$")
    public void theUserClicksOnTheLink(String link) throws InterruptedException {

        homePage_mlcinsurance.clickLinkByText(link);
        assertTrue("page is not displayed with the title LifeView",
                homePage_mlcinsurance.isLifeViewHeaderDisplayed("LifeView"));

    }

    @Then("^the page is displayed with the expected bread crumb$")
    public void thePageIsDisplayedWithBreadcrumbs() throws InterruptedException {
        assertTrue("All expected bread crumbs are displayed in the order",
                homePage_mlcinsurance.verifyExpectedBreadcrumbsDisplayed());
    }

    @When("^the user clicks on \"([^\"]*)\" button$")
    public void theUserClicksOnTheButton(String btn) throws InterruptedException {
        homePage_mlcinsurance.clickButtonByText(btn);
    }

    @Then("^the form \"([^\"]*)\" is displayed$")
    public void theFormIsDisplayed(String headerTxt) throws InterruptedException {
        assertTrue("page displayed with the title " + headerTxt,
                lifeViewDemoPage_mlcinsurance.isHeaderDisplayed(headerTxt));
    }

    @Then("^the user enters relevant data in the form$")
    public void theUserEntersRelevantDataInTheForm() {
        lifeViewDemoPage_mlcinsurance.fillDemoForm();
    }
}

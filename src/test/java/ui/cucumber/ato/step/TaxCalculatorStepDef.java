package ui.cucumber.ato.step;

import common.BaseTest;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;


public class TaxCalculatorStepDef extends BaseTest {

    private pages.ato.TaxCalculatorPage taxCalculatorPage_pageobject;


    @cucumber.api.java.en.Given("^Navigate to ATO questions page$")
    public void navigateToATOQuestionsPage() {

        taxCalculatorPage_pageobject.navigate();
        System.out.println("ATO questions page title is..." + taxCalculatorPage_pageobject.pageTitle());

    }

    @When("^user selects the income year \"([^\"]*)\" taxable income \"([^\"]*)\" and residency status \"([^\"]*)\"$")
    public void userSelectsTheIncomeYearTaxableIncomeAndResidencyStatus(String year, String income, String status)
            throws InterruptedException {

        taxCalculatorPage_pageobject.selectFinancialYear(year);
        taxCalculatorPage_pageobject.enterTaxableIncome(income);
        taxCalculatorPage_pageobject.selectResidencyStatus(status);
        System.out.println("residency status from the feature file is..." + status);

    }

    @Then("^user submit the calculator$")
    public void userSubmitTheCalculator() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(),2);
        taxCalculatorPage_pageobject.submitTaxCalculation();
        wait.until(ExpectedConditions.urlContains("report"));

    }

    @Then("^the tax calculator result is displayed with \"([^\"]*)\"$")
    public void theTaxCalculatorResultIsDisplayed(String amount) {
        System.out.println("Estimated tax amount is..." + taxCalculatorPage_pageobject.estimatedTaxAmount());
        assertTrue("Verify estimated tax amount not matching", taxCalculatorPage_pageobject.estimatedTaxAmount().contains(amount));

    }
}

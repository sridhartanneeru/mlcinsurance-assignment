Feature: Tax calculationwith user's taxable income, residfency status and income year

        @ato
        Scenario Outline: Estimated tax calculation for income year <year> with taxable income <income> and residency status <status>
            Given Navigate to ATO questions page
             When user selects the income year "<year>" taxable income "<income>" and residency status "<status>"
              And user submit the calculator
             Then the tax calculator result is displayed with "<amount>"
        Examples:
                  | year | income | status                     | amount    |
                  | 2019 | 100000 | Resident for full year     | 24,497.00 |
                  | 2018 | 50000  | Non-resident for full year | 16,250.00 |
                  | 2017 | 40000  | Part-year resident         | 4,996.92  |
                #   | 2018 | 100000 | Resident for full year     | 24,632.00 |
                #   | 2019 | 50000  | Non-resident for full year | 16,250.00 |
                #   | 2019 | 40000  | Part-year resident         | 4,996.92  |
                #   | 2017 | 100000 | Resident for full year     | 24,632.00 |
                #   | 2019 | 50000  | Non-resident for full year | 16,250.00 |
                #   | 2018 | 40000  | Part-year resident         | 4,996.92  |
Feature: Accessing Life view and fill the request a demo form

        @mlc
        Scenario: Accessing the Life view and fill the request a demo form
            Given Navigate to mlc insurance home page
              And the user searches for "LifeView"
              And the user clicks on "LifeView" link
             Then the page is displayed with the expected bread crumb
             When the user clicks on "Request a demo" button
             Then the form "Request a LifeView demo" is displayed
              And the user enters relevant data in the form
Feature: Australia Post API to calculate shipping costs for parcels of different weights to different countries

        @api
        Scenario Outline: Verifying API response with extimated amount for shipping to different countries and weights
            Given Send an api request with from country "<fromCountry>" to country "<toCountry>" and weight "<weight>"
             When GET Api returns response with status code "<apiResponseCode>"
             Then Validate the api response with "<estimatedPrice>" for "<weight>"
        Examples:
                  | fromCountry | toCountry | weight | apiResponseCode | estimatedPrice |
                  | AUS         | NZ        | 10000  | 200             | 159.2          |
                  | AUS         | GB        | 5000   | 200             | 163.3          |
                  | AUS         | US        | 2500   | 200             | 137.1          |

package features.api.test;

import io.restassured.response.Response;
import java.util.*;

import common.CoreConstant;
import net.serenitybdd.rest.SerenityRest;

public class GetAmountByWeightApi {

    public Response getAmountByWeight(String CategoryType, String fromCountry, String toCountry, String weight) {

        Map<String, String> parameters = new HashMap<>();
        parameters.put("category", CategoryType);
        parameters.put("from", fromCountry);
        parameters.put("to", toCountry);
        parameters.put("code", weight);

        return SerenityRest.given().accept("application/json").and()
                .header("AUTH-KEY", "62b9613ddab3f8cdaf89c47c0234729e").and().params(parameters).when()
                .get(CoreConstant.AUSPOST_API);

    }

}

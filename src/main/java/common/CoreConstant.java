package common;

import com.typesafe.config.Config;

public class CoreConstant {

    private static Config conf;

    static {
        conf = Configs.newBuilder().withResource("automation.conf").build();
    }

    public static final String AUSPOST_API = conf.getString("auspost.api");
    public static final String ATO_URL_QUESTIONS = conf.getString("ato.url");
    public static final String MLCINSURANCE_URL_HOMEPAGE = conf.getString("mlcinsurance.url");

}

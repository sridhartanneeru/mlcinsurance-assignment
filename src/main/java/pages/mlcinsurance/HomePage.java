package pages.mlcinsurance;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static common.CoreConstant.MLCINSURANCE_URL_HOMEPAGE;

public class HomePage extends PageObject {

    @FindBy(xpath = "//button[@data-popover-id='global-search']")
    private WebElementFacade btnGloablSearch;

    @FindBy(xpath = "//input[@type='search']")
    private WebElementFacade inputTypeSearch;

    @FindBy(xpath = "//h1[contains(text(),'Search results')]")
    private WebElementFacade resultTxt;

    @FindBy(xpath = "//div/a[@href='/partnering-with-us/superannuation-funds/lifeview']")
    private WebElementFacade searchLink;

    @FindBy(xpath = "//ul[@itemprop='breadcrumb']")
    private List<WebElementFacade> listBreadCrumb;

    public void clickLinkByText(String txt) {
        getDriver().findElement(By.xpath("//h2[text()=" + "'" + txt + "'" + "]")).click();
    }

    public void clickButtonByText(String txt) {
        getDriver().findElement(By.xpath("//span[text()=" + "'" + txt + "'" + "]")).click();
    }

    public void navigate() {
        getDriver().get(MLCINSURANCE_URL_HOMEPAGE);
    }

    public String getPageTitle() {
        return getDriver().getTitle();
    }

    public void searchGlobal(String searchWord) {
        btnGloablSearch.click();
        inputTypeSearch.isDisplayed();
        inputTypeSearch.sendKeys(searchWord);
        inputTypeSearch.sendKeys(Keys.ENTER);
    }

    public Boolean searchResultDisplayed() {
        return resultTxt.isDisplayed();

    }

    public void clickOnLink() {
        searchLink.isDisplayed();
        searchLink.click();
    }

    public Boolean verifyExpectedBreadcrumbsDisplayed() {
        String[] expected = { "Home Partnering with us Superannuation funds LifeView" };
        List<WebElementFacade> allOptions = listBreadCrumb;
        boolean result = false;

        // make sure to check the expected vs actual number of elements are same
        if (expected.length != allOptions.size()) {
            System.out.println("fail, wrong number of elements found");
        }
        // make sure that the value of every <option> element equals the expected value
        for (int i = 0; i < expected.length; i++) {
            String optionValue = allOptions.get(i).getText();
            if (optionValue.equals(expected[i])) {
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }

    public Boolean isLifeViewHeaderDisplayed(String txt) {

        return getDriver().findElement(By.xpath("//header/h1[text()=" + "'" + txt + "'" + "]")).isDisplayed();
    }

}

package pages.mlcinsurance;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class LifeViewDemoPage extends PageObject {

    @FindBy(xpath = "//label[text()='Name']/following-sibling::input")
    private WebElementFacade name;

    @FindBy(xpath = "//label[text()='Company']/following-sibling::input")
    private WebElementFacade company;

    @FindBy(xpath = "//label[text()='Email']/following-sibling::input")
    private WebElementFacade email;

    @FindBy(xpath = "//label[text()='Phone']/following-sibling::input")
    private WebElementFacade phone;

    @FindBy(xpath = "//input[@value='PM']/ancestor::label")
    private WebElementFacade preferredContactTimePM;

    @FindBy(xpath = "//input[@value='AM']/ancestor::label")
    private WebElementFacade preferredContactTimeAM;

    @FindBy(xpath = "//label[text()='Request details']/following-sibling::textarea")
    private WebElementFacade requestDetails;

    public void fillDemoForm() {
        name.sendKeys("test Name");
        company.sendKeys("MLC Insurance");
        email.sendKeys("test@mlcinsurance.com.au");
        phone.sendKeys("0444444445");
        preferredContactTimePM.click();
        requestDetails.sendKeys("this is for MLC insurance assessment only");
    }

    public Boolean isHeaderDisplayed(String txt) {

        return getDriver().findElement(By.xpath("//header/h1[text()=" + "'" + txt + "'" + "]")).isDisplayed();
    }

}

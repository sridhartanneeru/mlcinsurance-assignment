package pages.ato;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import org.openqa.selenium.support.ui.Select;
import static common.CoreConstant.ATO_URL_QUESTIONS;

public class TaxCalculatorPage extends PageObject {

    @FindBy(xpath = "//select[@id='ddl-financialYear']")
    private WebElementFacade dropdownIncomeYear;

    @FindBy(id = "texttaxIncomeAmt")
    private WebElementFacade inputIncomeAmount;

    @FindBy(xpath = "//label[@for='vrb-residentresident']")
    public WebElementFacade radioFullResident;

    @FindBy(xpath = "//label[@for='vrb-residentnonResident']")
    private WebElementFacade radioNonResident;

    @FindBy(xpath = "//label[@for='vrb-residentpartYearResident']")
    private WebElementFacade radioPartYearResident;

    @FindBy(xpath = "//select[@id='ddl-residentPartYearMonths']")
    private WebElementFacade dropdownNumberOfMonths;

    @FindBy(xpath = "//button[text()='Submit']")
    private WebElementFacade btnSubmit;

    @FindBy(xpath = "//span[@data-bind='text: formattedTaxPayable']")
    private WebElementFacade txtEstimatedTaxAmount;

    Select dropdown;

    public void navigate() {
        getDriver().get(ATO_URL_QUESTIONS);
    }

    public String pageTitle() {
        return getDriver().getTitle();
    }

    public void selectFinancialYear(final String finanicalYear) {
        waitFor(dropdownIncomeYear);
        dropdown = new Select(dropdownIncomeYear);
        dropdown.selectByValue(finanicalYear);
    }

    public void enterTaxableIncome(final String income) {
        waitFor(inputIncomeAmount);
        inputIncomeAmount.clear();
        inputIncomeAmount.sendKeys(income);
    }

    public void selectNumberOfMonths(String numberOfMonths) {
        waitFor(dropdownNumberOfMonths);
        dropdown = new Select(dropdownNumberOfMonths);
        dropdown.selectByValue(numberOfMonths);
    }

    public void selectResidencyStatus(final String status) {
        switch (status.toLowerCase()) {
            case "resident for full year":
                radioFullResident.click();
                break;
            case "non-resident for full year":
                radioNonResident.click();
                break;
            case "part-year resident":
                radioPartYearResident.click();
                waitFor(dropdownNumberOfMonths);
                selectNumberOfMonths("6");
                break;
            default:
                System.out.println("no selection available for residency status");
                break;

        }
    }

    public void submitTaxCalculation() {
        waitFor(btnSubmit);
        btnSubmit.click();
    }

    public String estimatedTaxAmount() {
        return txtEstimatedTaxAmount.getText();
    }

}
